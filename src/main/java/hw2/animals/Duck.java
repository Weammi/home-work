package hw2.animals;

public class Duck extends Carnivorous implements DuckAbilities, Voice, Swim {

    @Override
    public String swim() {
        return "I go to swim on the lake";
    }

    @Override
    public String voice() {
        return "Кря Кря Кря";
    }

    @Override
    public void whirl() {
        System.out.println("Кружусь, Кружусь");
    }

    @Override
    public Boolean game() {
        System.out.println("Мы утки - любим играть");
        energy--;
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Мы утки любим спать");
        energy--;
        return true;
    }
}
