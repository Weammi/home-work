package hw2.animals;

public class Cow extends Herbivore implements CowAbilities, Voice {

    @Override
    public void walkInTheField() {
        System.out.println("I go to walk in the field");
    }

    @Override
    public String voice() {
        return "Мууу Мууу Мууу";
    }

    @Override
    public Boolean game() {
        System.out.println("Мы коровы - любим играть в игры");
        energy--;
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Мы коровы иногда спим");
        energy--;
        return true;
    }
}
