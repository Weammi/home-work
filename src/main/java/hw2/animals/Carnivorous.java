package hw2.animals;

import hw2.food.Food;
import hw2.food.Meaty;

public class Carnivorous extends Animal {

    @Override
    public void eat(Food food) {
        if (food instanceof Meaty) {
            System.out.println("Мы не любим кушать траву!" + food);
        } else {
            System.out.println("Спасибо, но я это кушать не буду");
        }
    }

    @Override
    public Boolean game() {
        System.out.println("Хищники любят играть в догонялки");
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Хищники чутко спят");
        return true;
    }
}
