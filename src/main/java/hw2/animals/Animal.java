package hw2.animals;

import hw2.food.Food;

public abstract class Animal {
    protected int energy;

    public void eat(Food food) {
        System.out.println("Всем существам нужна пища");
    }

    public abstract Boolean game();

    public abstract Boolean sleep();

}

