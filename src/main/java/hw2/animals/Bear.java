package hw2.animals;

public class Bear extends Carnivorous implements BearAbilities, Voice {

    @Override
    public void lookForHoney() {
        System.out.println("I go to look for mead");
    }

    @Override
    public String voice() {
        return "mmm.... Mead";
    }

    @Override
    public Boolean game() {
        energy--;
        System.out.println("Мы медведи - любим играть в игры");
        return true;
    }

    @Override
    public Boolean sleep() {
        energy--;
        System.out.println("Медведи очень любят спать");
        return true;
    }
}
