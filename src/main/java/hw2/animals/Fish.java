package hw2.animals;

public class Fish extends Herbivore implements FishAbilities, Swim {
    @Override
    public String swim() {
        return "I go to swim! I like swim!";
    }

    @Override
    public void swimBehindTheFlock() {
        System.out.println("Когда нас много, нам весело");
    }

    @Override
    public Boolean game() {
        System.out.println("Мы рыбы - любим играть");
        energy--;
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Мы рыбы - любим спать");
        energy--;
        return true;
    }
}
