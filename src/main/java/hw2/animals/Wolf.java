package hw2.animals;

public class Wolf extends Carnivorous implements WolfAbilities, Voice {
    @Override
    public void hunt() {
        System.out.println("Looking for loot");
    }

    @Override
    public String voice() {
        return "Ну, погоди!";
    }

    @Override
    public Boolean game() {
        System.out.println("Мы волки - любим играть в игры");
        energy--;
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Мы волки - редко спим");
        energy--;
        return true;
    }
}
