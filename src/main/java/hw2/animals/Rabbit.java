package hw2.animals;

public class Rabbit extends Herbivore implements RabbitAbilities, Voice {
    @Override
    public void run() {
        System.out.println("Well i ran");
    }

    @Override
    public String voice() {
        return "Морковку будешь?";
    }

    @Override
    public Boolean game() {
        System.out.println("Мы кролики - очень любим играть");
        energy--;
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Мы кролики - редко спим");
        energy--;
        return true;
    }
}
