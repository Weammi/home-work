package hw2.animals;

import hw2.food.Food;

public class Worker {
    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public void getVoice(Voice voice) {
        System.out.println(voice.voice());
    }

    public void liveAnotherDay(Animal animal) {
        for (int i = 0; i <= 23; i++) {
            if (animal.energy == 0) {
                System.out.println("I go to eat");
                animal.energy = animal.energy + 5;
                System.out.println("I ate");
            } else {
                if (Math.random() < 0.5) {
                    animal.game();
                } else {
                    animal.sleep();
                }
            }
        }
    }
}
