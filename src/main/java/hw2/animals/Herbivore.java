package hw2.animals;

import hw2.food.Food;
import hw2.food.Veggie;

public class Herbivore extends Animal {

    @Override
    public void eat(Food food) {
        if (food instanceof Veggie) {
            System.out.println("Мы не любим кушать мясо!" + food);
        } else {
            System.out.println("Спасибо, но я это кушать не буду");
        }
    }

    @Override
    public Boolean game() {
        System.out.println("Травоядные любят играть с растениями");
        return true;
    }

    @Override
    public Boolean sleep() {
        System.out.println("Травоядные любят спать)");
        return true;
    }
}
