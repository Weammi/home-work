package hw2;

import hw2.animals.*;
import hw2.food.*;

import java.util.ArrayList;

public abstract class Zoo {
    public static void main(String[] args) {
        ArrayList<Swim> pond = new ArrayList();
        MeatSoup meatSoup = new MeatSoup("Мясосуп");
        VegetableSalad vegetableSalad = new VegetableSalad("Вегетарианский салат");
        Ratatouille ratatouille = new Ratatouille("Рататуй");
        Barbecue barbecue = new Barbecue("Шашлычек");
        Worker worker = new Worker();
        Rabbit rabbit = new Rabbit();
        Duck duck = new Duck();
        Fish fish = new Fish();
        Wolf wolf = new Wolf();
        Cow cow = new Cow();
        Bear bear = new Bear();

        worker.getVoice(duck);
        worker.getVoice(wolf);
        worker.feed(fish, ratatouille);
        worker.feed(wolf, barbecue);
        worker.feed(cow, vegetableSalad);
        worker.feed(rabbit, vegetableSalad);
        worker.feed(bear, meatSoup);
        worker.feed(bear, vegetableSalad);
        worker.feed(rabbit, meatSoup);
        worker.feed(wolf, ratatouille);
        worker.feed(bear, vegetableSalad);
        worker.feed(fish, meatSoup);
        worker.feed(cow, barbecue);
        worker.getVoice(bear);
        worker.getVoice(duck);
        worker.getVoice(wolf);
        worker.getVoice(rabbit);

        pond.add(duck);
        pond.add(duck);
        pond.add(fish);
        pond.add(fish);
        pond.add(fish);
        pond.add(fish);
        pond.add(fish);
        pond.add(duck);
        pond.add(duck);

        for (int i = 0; i < pond.size(); i++) {
            System.out.println(pond.get(i).swim());
        }

        worker.liveAnotherDay(bear);
        worker.liveAnotherDay(duck);
    }
}
