package hw2.food;

public class Food {
    public String nameFood;

    public Food(String nameFood) {
        this.nameFood = nameFood;
    }

    @Override
    public String toString() {
        return " Спасибо за " + nameFood + "!";
    }
}
