package hw1;

import hw1.model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik barsik = new Kotik("Puh", 22, "I have to eat", 5);
        barsik.eat();
        Kotik murchik = new Kotik();
        murchik.setKotik("Vinni", 20, "I have to eat", 4);
        barsik.liveAnotherDay();
        System.out.println("Имя: " + barsik.getName() + " " + "Вес:" + barsik.getWeight());
        System.out.println("Сравнение Meow у котов: " + barsik.getMeow().equals(murchik.getMeow()));
        System.out.println("InstancesKotik = " + Kotik.getKotikInstances());
    }
}
