package hw1.model;

public class Kotik {

    private int catEnergy;
    private String name;
    private int weight;
    private String meow;
    private int prettiness;
    private static int kotikInstances;

    public Kotik(String name, int weight, String meow, int prettiness) {
        kotikInstances++;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.prettiness = prettiness;
    }

    public Kotik() {
        kotikInstances++;
    }

    public void setKotik(String name, int weight, String meow, int prettiness) {
        this.name = name;
        this.meow = meow;
        this.weight = weight;
        this.prettiness = prettiness;
    }

    public void liveAnotherDay() {
        catEnergy = 5;
        for (int i = 0; i <= 23; i++) {
            if (energyCheck()) {
                System.out.println("I go to eat");
                eat(5);
                System.out.println("I ate");
            } else {
                switch (random()) {
                    case 1:
                        play();
                        break;
                    case 2:
                        sleep();
                        break;
                    case 3:
                        chaseMouse();
                        break;
                    case 4:
                        walk();
                        break;
                    case 5:
                        rest();
                        break;
                }
            }
        }
    }

    public Boolean energyCheck(){
        if (catEnergy==0){
            return true;
        }else {
            return false;
        }
    }


    public void eat(int satiety) {
        catEnergy = satiety + catEnergy;
    }

    public void eat(int satiety, String foodName) {
        eat(satiety);
        System.out.println(foodName);
    }

    public void eat() {
        eat(5, "Viskase");
    }

    public int random() {
        int a = 1, b = 5;
        int randomNubmer = a + (int) (Math.random() * b);
        return randomNubmer;
    }

    public boolean play() {
        System.out.println("I game");
        catEnergy--;
        return true;
    }

    public boolean sleep() {
        System.out.println("I sleep");
        catEnergy--;
        return true;
    }

    public boolean chaseMouse() {
        System.out.println("I chaseMouse");
        catEnergy--;
        return true;
    }

    public boolean walk() {
        System.out.println("I walk");
        catEnergy--;
        return true;
    }

    public boolean rest() {
        System.out.println("I rest");
        catEnergy--;
        return true;
    }

    public static int getKotikInstances() {
        return kotikInstances;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public int getPrettiness() {
        return prettiness;
    }
}
